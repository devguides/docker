# Example on How to Use Docker Container Registry with Gitlab

This code base is used to illustrate how you can push your docker images to Gitlab container registry in your build pipeline.
For a full explanation you can read the guide over at [devguides.dev](https://www.devguides.dev/build-and-push-your-docker-images-to-gitlab-container-registry/)
